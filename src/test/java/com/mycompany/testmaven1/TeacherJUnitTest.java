/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testmaven1;

import models.Teachers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Salahiddinov
 */
public class TeacherJUnitTest {
    
    public TeacherJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void GetTeachersInfo()
     {
         //Given name:
        String name = "TestName";
        String lname = "LastnameTEst";
        //when:
        Teachers teach = new Teachers();
        teach.setFirstname(name);
        teach.setLastname(lname);
        //then:
        Assert.assertTrue(teach.getFirstname().equals(name));
        Assert.assertTrue(teach.getLastname().equals(lname));
        Assert.assertSame(teach.getFirstname(), name);
        
     }
}

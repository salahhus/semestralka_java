 package Server;
 
// import potřebných balíčků
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author Salahiddinov
 */

public class Server {
    // privátní atribut serverSocket
    private ServerSocket serverSocket;
    private ArrayList<BufferedReader> clientBufReaders;

    public static void main(String[] args) {
        Server server = new Server();
    }
 // konstruktor třídy Server
    public Server() {
        try {
            this.serverSocket = new ServerSocket(5435); // inicializace ServerSocketu
            System.out.print("Spuštění serveru proběhlo úspěšně.\nČekám na připojení klienta...\n"); // vypsání hlášky při úspěšném spuštění k serveru
            this.clientBufReaders = new ArrayList<BufferedReader>();

            this.clients();
        } catch (IOException e ) {
            e.printStackTrace();
        }
    }

    private void clients() {
        Thread acceptThread = new Thread(new Runnable() {
            public void run() {
                while(true) {
                    try {
                        Socket clientSocket = serverSocket.accept(); // vytvoření socketu pro klienta
                        // vytvoření BufferReaderu, se vstupním proudem se socketu klienta
                        clientBufReaders.add(new BufferedReader(new InputStreamReader(clientSocket.getInputStream())));
                        System.out.println("Klient se připojil.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        acceptThread.start();
 // nekonečný while cyklus pro vypisování zpráv od klienta
        while(true) {
            synchronized(clientBufReaders) {
                for(BufferedReader in :  clientBufReaders) {
                    try {
                        if(in.ready()) {
                            System.out.println(in.readLine()); //načtení zprávy od klienta do proměnné 
                        } else {
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}


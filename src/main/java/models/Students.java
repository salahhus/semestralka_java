/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.log4j.*;

/**
 *
 * @author Salahiddinov
 */
@Entity
@Table(name = "students")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Students.findAll", query = "SELECT s FROM Students s"),
    @NamedQuery(name = "Students.findByIdStudent", query = "SELECT s FROM Students s WHERE s.idStudent = :idStudent"),
    @NamedQuery(name = "Students.findByFirstname", query = "SELECT s FROM Students s WHERE s.firstname = :firstname"),
    @NamedQuery(name = "Students.findByLastname", query = "SELECT s FROM Students s WHERE s.lastname = :lastname"),
    @NamedQuery(name = "Students.findByAdStreet", query = "SELECT s FROM Students s WHERE s.adStreet = :adStreet"),
    @NamedQuery(name = "Students.findByAdNumber", query = "SELECT s FROM Students s WHERE s.adNumber = :adNumber"),
    @NamedQuery(name = "Students.findByHealthCondition", query = "SELECT s FROM Students s WHERE s.healthCondition = :healthCondition"),
    @NamedQuery(name = "Students.findByInfoStudent", query = "SELECT s FROM Students s WHERE s.infoStudent = :infoStudent")})
public class Students implements Serializable {
 //create logger:
     final  static Logger LOGGER = Logger.getLogger(City.class);
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_student")
    private Double idStudent;
    @Column(name = "firstname")
    private String firstname;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "ad_street")
    private String adStreet;
    @Column(name = "ad_number")
    private Integer adNumber;
    @Column(name = "health_condition")
    private Boolean healthCondition;
    @Column(name = "info_student")
    private String infoStudent;
    @JoinColumn(name = "id_psc", referencedColumnName = "id_city")
    @ManyToOne
    private City idPsc;
    @JoinColumn(name = "id_course", referencedColumnName = "id_course")
    @ManyToOne
    private Courses idCourse;
    @OneToMany(mappedBy = "rcStudent")
    private Collection<Profit> profitCollection;

    public Students() {
    }

    public Students(Double idStudent) {
        this.idStudent = idStudent;
    }

    Students(int aInt, String string, String string0, String string1, int aInt0, int aInt1, int aInt2, boolean aBoolean, String string2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Double getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Double idStudent) {
        Double oldIdStudent = this.idStudent;
        this.idStudent = idStudent;
        changeSupport.firePropertyChange("idStudent", oldIdStudent, idStudent);
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        String oldFirstname = this.firstname;
        this.firstname = firstname;
        changeSupport.firePropertyChange("firstname", oldFirstname, firstname);
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        String oldLastname = this.lastname;
        this.lastname = lastname;
        changeSupport.firePropertyChange("lastname", oldLastname, lastname);
    }

    public String getAdStreet() {
        return adStreet;
    }

    public void setAdStreet(String adStreet) {
        String oldAdStreet = this.adStreet;
        this.adStreet = adStreet;
        changeSupport.firePropertyChange("adStreet", oldAdStreet, adStreet);
    }

    public Integer getAdNumber() {
        return adNumber;
    }

    public void setAdNumber(Integer adNumber) {
        Integer oldAdNumber = this.adNumber;
        this.adNumber = adNumber;
        changeSupport.firePropertyChange("adNumber", oldAdNumber, adNumber);
    }

    public Boolean getHealthCondition() {
        return healthCondition;
    }

    public void setHealthCondition(Boolean healthCondition) {
        Boolean oldHealthCondition = this.healthCondition;
        this.healthCondition = healthCondition;
        changeSupport.firePropertyChange("healthCondition", oldHealthCondition, healthCondition);
    }

    public String getInfoStudent() {
        return infoStudent;
    }

    public void setInfoStudent(String infoStudent) {
        String oldInfoStudent = this.infoStudent;
        this.infoStudent = infoStudent;
        changeSupport.firePropertyChange("infoStudent", oldInfoStudent, infoStudent);
    }

    public City getIdPsc() {
        return idPsc;
    }

    public void setIdPsc(City idPsc) {
        City oldIdPsc = this.idPsc;
        this.idPsc = idPsc;
        changeSupport.firePropertyChange("idPsc", oldIdPsc, idPsc);
    }

    public Courses getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(Courses idCourse) {
        Courses oldIdCourse = this.idCourse;
        this.idCourse = idCourse;
        changeSupport.firePropertyChange("idCourse", oldIdCourse, idCourse);
    }

    @XmlTransient
    public Collection<Profit> getProfitCollection() {
        return profitCollection;
    }

    public void setProfitCollection(Collection<Profit> profitCollection) {
        this.profitCollection = profitCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStudent != null ? idStudent.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Students)) {
            return false;
        }
        Students other = (Students) object;
        if ((this.idStudent == null && other.idStudent != null) || (this.idStudent != null && !this.idStudent.equals(other.idStudent))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.testmaven1.Students[ idStudent=" + idStudent + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}

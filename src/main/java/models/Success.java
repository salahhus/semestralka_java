/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.log4j.Logger;

/**
 *
 * @author Salahiddinov
 */
@Entity
@Table(name = "success")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Success.findAll", query = "SELECT s FROM Success s"),
    @NamedQuery(name = "Success.findByIdList", query = "SELECT s FROM Success s WHERE s.idList = :idList"),
    @NamedQuery(name = "Success.findByRcStudent", query = "SELECT s FROM Success s WHERE s.rcStudent = :rcStudent"),
    @NamedQuery(name = "Success.findBySuccess", query = "SELECT s FROM Success s WHERE s.success = :success")})
public class Success implements Serializable {
 //create logger:
     final  static Logger LOGGER = Logger.getLogger(City.class);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_list")
    private Integer idList;
    @Column(name = "rc_student")
    private Integer rcStudent;
    @Column(name = "success")
    private String success;

    public Success() {
    }

    public Success(Integer idList) {
        this.idList = idList;
    }

    public Integer getIdList() {
        return idList;
    }

    public void setIdList(Integer idList) {
        this.idList = idList;
    }

    public Integer getRcStudent() {
        return rcStudent;
    }

    public void setRcStudent(Integer rcStudent) {
        this.rcStudent = rcStudent;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idList != null ? idList.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Success)) {
            return false;
        }
        Success other = (Success) object;
        if ((this.idList == null && other.idList != null) || (this.idList != null && !this.idList.equals(other.idList))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.testmaven1.Success[ idList=" + idList + " ]";
    }
    
}

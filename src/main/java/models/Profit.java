/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import models.Subjects;
import models.Teachers;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.log4j.Logger;

/**
 *
 * @author Salahiddinov
 */
@Entity
@Table(name = "profit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profit.findAll", query = "SELECT p FROM Profit p"),
    @NamedQuery(name = "Profit.findByIdProfit", query = "SELECT p FROM Profit p WHERE p.idProfit = :idProfit"),
    @NamedQuery(name = "Profit.findByDate", query = "SELECT p FROM Profit p WHERE p.date = :date"),
    @NamedQuery(name = "Profit.findByInfoScore", query = "SELECT p FROM Profit p WHERE p.infoScore = :infoScore")})
public class Profit implements Serializable {
 //create logger:
     final  static Logger LOGGER = Logger.getLogger(City.class);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_profit")
    private Integer idProfit;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "info_score")
    private String infoScore;
    @JoinColumn(name = "id_score", referencedColumnName = "id_score")
    @ManyToOne
    private Score idScore;
    @JoinColumn(name = "rc_student", referencedColumnName = "id_student")
    @ManyToOne
    private Students rcStudent;
    @JoinColumn(name = "id_subject", referencedColumnName = "id_subject")
    @ManyToOne
    private Subjects idSubject;
    @JoinColumn(name = "rc_teacher", referencedColumnName = "rc_teacher")
    @ManyToOne
    private Teachers rcTeacher;

    public Profit() {
    }

    public Profit(Integer idProfit) {
        this.idProfit = idProfit;
    }

    public Integer getIdProfit() {
        return idProfit;
    }

    public void setIdProfit(Integer idProfit) {
        this.idProfit = idProfit;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getInfoScore() {
        return infoScore;
    }

    public void setInfoScore(String infoScore) {
        this.infoScore = infoScore;
    }

    public Score getIdScore() {
        return idScore;
    }

    public void setIdScore(Score idScore) {
        this.idScore = idScore;
    }

    public Students getRcStudent() {
        return rcStudent;
    }

    public void setRcStudent(Students rcStudent) {
        this.rcStudent = rcStudent;
    }

    public Subjects getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(Subjects idSubject) {
        this.idSubject = idSubject;
    }

    public Teachers getRcTeacher() {
        return rcTeacher;
    }

    public void setRcTeacher(Teachers rcTeacher) {
        this.rcTeacher = rcTeacher;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProfit != null ? idProfit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profit)) {
            return false;
        }
        Profit other = (Profit) object;
        if ((this.idProfit == null && other.idProfit != null) || (this.idProfit != null && !this.idProfit.equals(other.idProfit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.testmaven1.Profit[ idProfit=" + idProfit + " ]";
    }
    
}

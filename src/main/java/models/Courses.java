/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import models.Students;
import models.Subjects;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.log4j.Logger;

/**
 *
 * @author Salahiddinov
 */
@Entity
@Table(name = "courses")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Courses.findAll", query = "SELECT c FROM Courses c"),
    @NamedQuery(name = "Courses.findByIdCourse", query = "SELECT c FROM Courses c WHERE c.idCourse = :idCourse"),
    @NamedQuery(name = "Courses.findByNumCourse", query = "SELECT c FROM Courses c WHERE c.numCourse = :numCourse"),
    @NamedQuery(name = "Courses.findByNameCourse", query = "SELECT c FROM Courses c WHERE c.nameCourse = :nameCourse")})
public class Courses implements Serializable {
 //create logger:
     final  static Logger LOGGER = Logger.getLogger(City.class);
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_course")
    private Integer idCourse;
    @Column(name = "num_course")
    private Integer numCourse;
    @Column(name = "name_course")
    private String nameCourse;
    @OneToMany(mappedBy = "idCourse")
    private Collection<Subjects> subjectsCollection;
    @OneToMany(mappedBy = "idCourse")
    private Collection<Students> studentsCollection;

    public Courses() {
    }

    public Courses(Integer idCourse) {
        this.idCourse = idCourse;
    }

    public Integer getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(Integer idCourse) {
        Integer oldIdCourse = this.idCourse;
        this.idCourse = idCourse;
        changeSupport.firePropertyChange("idCourse", oldIdCourse, idCourse);
    }

    public Integer getNumCourse() {
        return numCourse;
    }

    public void setNumCourse(Integer numCourse) {
        Integer oldNumCourse = this.numCourse;
        this.numCourse = numCourse;
        changeSupport.firePropertyChange("numCourse", oldNumCourse, numCourse);
    }

    public String getNameCourse() {
        return nameCourse;
    }

    public void setNameCourse(String nameCourse) {
        String oldNameCourse = this.nameCourse;
        this.nameCourse = nameCourse;
        changeSupport.firePropertyChange("nameCourse", oldNameCourse, nameCourse);
    }

    @XmlTransient
    public Collection<Subjects> getSubjectsCollection() {
        return subjectsCollection;
    }

    public void setSubjectsCollection(Collection<Subjects> subjectsCollection) {
        this.subjectsCollection = subjectsCollection;
    }

    @XmlTransient
    public Collection<Students> getStudentsCollection() {
        return studentsCollection;
    }

    public void setStudentsCollection(Collection<Students> studentsCollection) {
        this.studentsCollection = studentsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCourse != null ? idCourse.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Courses)) {
            return false;
        }
        Courses other = (Courses) object;
        if ((this.idCourse == null && other.idCourse != null) || (this.idCourse != null && !this.idCourse.equals(other.idCourse))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.testmaven1.Courses[ idCourse=" + idCourse + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}

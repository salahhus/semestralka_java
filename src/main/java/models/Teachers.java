/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Salahiddinov
 */
@Entity
@Table(name = "teachers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Teachers.findAll", query = "SELECT t FROM Teachers t"),
    @NamedQuery(name = "Teachers.findByRcTeacher", query = "SELECT t FROM Teachers t WHERE t.rcTeacher = :rcTeacher"),
    @NamedQuery(name = "Teachers.findByFirstname", query = "SELECT t FROM Teachers t WHERE t.firstname = :firstname"),
    @NamedQuery(name = "Teachers.findByLastname", query = "SELECT t FROM Teachers t WHERE t.lastname = :lastname"),
    @NamedQuery(name = "Teachers.findByTitul", query = "SELECT t FROM Teachers t WHERE t.titul = :titul"),
    @NamedQuery(name = "Teachers.findByTelephone", query = "SELECT t FROM Teachers t WHERE t.telephone = :telephone"),
    @NamedQuery(name = "Teachers.findByEmail", query = "SELECT t FROM Teachers t WHERE t.email = :email"),
    @NamedQuery(name = "Teachers.findByDisabled", query = "SELECT t FROM Teachers t WHERE t.disabled = :disabled")})
public class Teachers implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "rc_teacher")
    private Double rcTeacher;
    @Column(name = "firstname")
    private String firstname;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "titul")
    private String titul;
    @Column(name = "telephone")
    private String telephone;
    @Column(name = "email")
    private String email;
    @Column(name = "disabled")
    private Boolean disabled;

    public Teachers() {
    }

    public Teachers(Double rcTeacher) {
        this.rcTeacher = rcTeacher;
    }

    Teachers(double aDouble, String string, String string0, String string1, String string2, String string3, boolean aBoolean) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Double getRcTeacher() {
        return rcTeacher;
    }

    public void setRcTeacher(Double rcTeacher) {
        this.rcTeacher = rcTeacher;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTitul() {
        return titul;
    }

    public void setTitul(String titul) {
        this.titul = titul;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rcTeacher != null ? rcTeacher.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Teachers)) {
            return false;
        }
        Teachers other = (Teachers) object;
        if ((this.rcTeacher == null && other.rcTeacher != null) || (this.rcTeacher != null && !this.rcTeacher.equals(other.rcTeacher))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.testmaven1.Teachers[ rcTeacher=" + rcTeacher + " ]";
    }
    
}
